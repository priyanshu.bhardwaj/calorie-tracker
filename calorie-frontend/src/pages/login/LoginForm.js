import React from "react";

const LoginForm = () => {
  return (
    <div className="flex flex-col gap-4">
      <div>
        <div className="text-xs font-medium mb-1">Email-ID</div>
        <input
          type="text"
          className="border-2 rounded-md outline-none p-2 w-full"
        />
      </div>
      <div>
        <div className="text-xs font-medium mb-1">Password</div>
        <input
          type="text"
          className="border-2 rounded-md outline-none p-2 w-full"
        />
        <div className="flex justify-end">
          <div className="text-xs font-medium text-gray-500 hover:text-black mt-1 cursor-pointer w-fit">
            Forgot password?
          </div>
        </div>
      </div>
      <button className="bg-black text-white p-2 rounded-md">Log in</button>
    </div>
  );
};

export default LoginForm;
