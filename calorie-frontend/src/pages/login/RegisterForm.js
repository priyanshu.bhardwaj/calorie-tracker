import React from "react";

const RegisterForm = () => {
  return (
    <div className="flex flex-col gap-4">
      <div>
        <div className="text-xs font-medium mb-1">Full name</div>
        <input
          type="text"
          className="border-2 rounded-md outline-none p-2 w-full"
        />
      </div>
      <div>
        <div className="text-xs font-medium mb-1">Email-ID</div>
        <input
          type="text"
          className="border-2 rounded-md outline-none p-2 w-full"
        />
      </div>
      <div>
        <div className="text-xs font-medium mb-1">Password</div>
        <input
          type="text"
          className="border-2 rounded-md outline-none p-2 w-full"
        />
      </div>
      <button className="bg-black text-white p-2 rounded-md">Register</button>
    </div>
  );
};

export default RegisterForm;
