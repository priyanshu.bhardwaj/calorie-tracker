import { NestFactory } from '@nestjs/core';
import { AppModule } from './app.module';
import {config} from 'aws-sdk'

async function bootstrap() {
  config.update({
    accessKeyId: process.env.AWS_ACCESS_KEY,
    secretAccessKey: process.env.AWS_SECRET_ACCESS_KEY,
    region: process.env.AWS_REGION
  })
  const app = await NestFactory.create(AppModule);
  app.enableCors()
  await app.listen(3001);
}
bootstrap();
