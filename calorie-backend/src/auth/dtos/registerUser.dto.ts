export class registerUserDto {
    name: string
    email: string
    picture: string
}