import { Body, Controller, Get, Patch, Post, Req} from '@nestjs/common';
import { AuthService } from './auth.service';
import { loginUserDto } from './dtos/loginUser.dto';
import { JwtService } from '@nestjs/jwt';
import { Request } from 'express';
import { Public } from 'src/decorators/public/public.decorator';

@Controller('/')
export class AuthController {
    constructor(private readonly authService: AuthService, private jwtService:JwtService ) { }
    
    @Public()
    @Post('login')
    async loginUser(@Body() loginData: loginUserDto) {
        let result = await this.authService.loginUser(loginData)
        if (!result) {
            result = await this.authService.registerUser(loginData)
        }
        const jwt = await this.jwtService.signAsync({ email: result.email, uid: result._id })
        return {
            token: jwt
        }
    }
    
    @Get('user')
    async getUserProfile(@Req() req:Request) {
        const userId = req['userId']
        const result = await this.authService.getUserProfile(userId)
        return result
    }

    @Patch('user')
    async updateUserProfile(@Body() userDetails:any, @Req() req:Request) {
         const userId = req['userId']
        const result = await this.authService.updateUserProfile(userId, userDetails)
        return result
    }
}
